\section{Explainable AI}
\label{sec:XAI_tech}

Explainable Artificial Intelligence (XAI) comprises methodologies and techniques aimed at making AI models' decision-making process transparent and understandable to humans. As AI algorithms, particularly neural networks, grow in complexity, XAI becomes vital in providing insights into how models arrive at specific predictions. By generating human-interpretable explanations, XAI enhances AI systems' accountability, fosters trust among users, enables developers to identify and rectify biases or errors, and assists auditors in validating models \cite{goodman2016eu,lundberg2017unified,datta2016algorithmic}.

The widespread adoption of AI systems in domains like healthcare, finance, and autonomous vehicles necessitates transparency in AI decision-making. XAI addresses this need, enabling stakeholders to comprehend the reasons behind model outputs and factors influencing predictions. This interpretability is critical in high-stakes applications where erroneous or biased decisions can have real-world consequences. By providing transparent insights, XAI promotes the responsible and ethical deployment of AI technologies, fostering acceptance and cooperation.

Transparent AI model decisions offer several advantages. Firstly, XAI establishes trust between users and AI systems, as understanding the reasoning behind predictions increases acceptance. This is crucial in sensitive areas like healthcare, where comprehensible diagnostic or treatment recommendations are vital. Secondly, transparent explanations facilitate effective model debugging and improvement. By identifying the causes of incorrect predictions or biases, developers can fine-tune models for better accuracy and fairness. Thirdly, XAI allows regulators to assess AI models' compliance with ethical and legal standards, ensuring fairness and accountability. Ultimately, XAI benefits individual users, developers, and promotes trustworthy AI integration in society.

In the following subsections, we will discuss two prominent XAI techniques that inherently incorporate XAI principles.



\subsection{SHAP (Shapley Additive exPlanations)}
\label{subsec:shap}

SHAP (SHapley Additive exPlanations) \cite{lundberg2017unified} is based on the concept of Shapley values \cite{winter2002shapley} from cooperative game theory. This method is a global feature importance technique that assigns a value to each feature in a predictive model, indicating its contribution to the model's prediction for a specific instance. SHAP values provide a comprehensive explanation of how each feature interacts with others and contributes to the final prediction. The Shapley value approach ensures that feature importance is fairly allocated among the features.

The SHAP value of feature $i$ is computed as follows:
\begin{equation}
\text{SHAP}(i) = \sum_{S \in \mathcal{P}_{-i}(N)} \frac{\text{marginal\_contribution}(S)}{n! \cdot (N - n - 1)!}
\end{equation}
where:
\begin{itemize}
    \item $\text{SHAP}(i)$: The SHAP value of feature $i$.
    \item $\mathcal{P}_{-i}(N)$: The set of all feature subsets that include feature $i$.
    \item $\text{marginal\_contribution}(S)$: The difference in the model's output when considering feature subset $S$ and the model's output when excluding feature subset $S$. It represents the contribution of the feature subset $S$ to the prediction.
    \item $n$: The number of features in subset $S$.
    \item $N$: The total number of features.
\end{itemize}

However, SHAP does have some limitations. One major disadvantage is its potential exponential growth in computation time, especially for models with numerous features. While approximation techniques mitigate this issue, they may introduce some level of approximation error. Moreover, SHAP values can be difficult to interpret for non-experts, as they rely on concepts from cooperative game theory. Consequently, effectively communicating SHAP-based explanations to lay audiences can be challenging. Additionally, SHAP values may not provide local explanations for specific instances, focusing more on the overall feature importance. In scenarios where localized explanations are crucial, this limitation could be a drawback. Despite these limitations, SHAP remains a powerful tool for global feature importance analysis and understanding the complex interactions among features in AI models.


\subsection{LIME (Local Interpretable Model-Agnostic Explanations)}
\label{subsec:LIME}

LIME (Local Interpretable Model-Agnostic Explanations) \cite{ribeiro2016should} offers valuable local explanations for individual instances. Its ability to approximate black-box models locally allows precise understanding of model predictions in specific contexts. Model-agnostic explanations make LIME versatile and applicable to various AI models. The simplicity of interpretable models used by LIME facilitates communication of explanations to diverse audiences. Visual explanations, such as heatmaps, enhance understanding of feature importance for specific predictions.

However, LIME has limitations. Explanations are local and may not accurately represent global model behavior. Choice of interpretable model and perturbation-based sampling influence explanation quality. Computationally expensive sampling can lead to increased explanation generation time. Despite these limitations, LIME is valuable for generating localized, human-interpretable explanations and understanding individual model predictions.
The LIME model's approximation for the original model's prediction for instance $x$ is given by:
\begin{equation}
\hat{f}_{\text{lime}}(x) = \arg\min_{g \in G} L(f, g, \pi_x) + \Omega(g)
\end{equation}
where:
\begin{itemize}
    \item $\hat{f}_{\text{lime}}(x)$: The LIME model's approximation for the original model's prediction for instance $x$.
    \item $f$: The original model being explained.
    \item $g$: Represents a simpler interpretable model, such as linear regression or decision trees, that approximates the original model.
    \item $G$: The set of possible interpretable models.
    \item $L(f, g, \pi_x)$: The local fidelity term measuring the closeness between the original model $f$ and interpretable model $g$ in the neighborhood of instance $x$, weighted by the proximity $\pi_x$ of each sampled instance to $x$.
    \item $\Omega(g)$: The complexity term encouraging the simplicity of the interpretable model $g$, often defined as regularization or complexity penalty.
\end{itemize}

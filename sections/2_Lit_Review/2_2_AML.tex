\section{Adversarial Machine Learning}
\label{sec:AML}

Adversarial Machine Learning (AML) involves exploiting vulnerabilities in AI systems to manipulate their behavior, leading to misclassification or misbehavior. These attacks create imperceptible perturbations in input data, challenging the reliability and security of AI systems \cite{ribeiro2016should,he2017adversarial,kumar2020adversarial}.

Adversarial attacks have far-reaching implications across various domains. For instance, in computer vision, they can manipulate images to deceive object recognition systems, impacting applications like autonomous driving and surveillance. In cybersecurity, these attacks aim to bypass machine learning-based intrusion detection systems or spam filters, underscoring the need for robust defenses and the significance of understanding vulnerabilities through eXplainable Artificial Intelligence (XAI) techniques.

\subsection{Fast Gradient Sign Method (FGSM)}
\label{subsec:FGSM}
The Fast Gradient Sign Method (FGSM) \cite{goodfellow2014explaining} is a computationally efficient technique used to craft adversarial examples in AML. It perturbs input data in the direction of the gradient of the loss function, aiming to maximize the loss and cause misclassification in the target model. Being model-specific, FGSM does not require access to the target model's architecture or parameters, making it a popular choice for crafting adversarial examples.

The adversarial example $x_{\text{adv}}$ is obtained by adding a small scalar value $\epsilon$ times the sign of the gradient of the loss function $J$ with respect to the input $x$:

\begin{equation}
x_{\text{adv}} = x + \epsilon \cdot \text{sign}(\nabla_x J(\theta, x, y))
\end{equation}

Where:
\begin{itemize}
\item $x$ is the original input instance.
\item $\epsilon$ is the perturbation magnitude.
\item $\nabla_x J(\theta, x, y)$ is the gradient of the loss function $J$ with respect to the input $x$, computed for the target model's parameters $\theta$ and the true label $y$.
\item $\text{sign}(\cdot)$ returns the sign of each element of the gradient vector.
\item $x_{\text{adv}}$ is the resulting adversarial example.
\end{itemize}

\subsection{Gradient Ascent Method (GAM)}
\label{subsec:GAM}
The Gradient Ascent Method (GAM) is an iterative optimization technique used to craft adversarial examples. It aims to maximize the loss function with respect to the input data, leading to misclassification or misbehavior of the target model. Unlike FGSM, GAM allows more fine-grained control over the perturbation magnitude, often producing stronger adversarial examples by adjusting the step size and the number of iterations.

At each iteration, the adversarial example $x_{\text{adv}}^{(t+1)}$ is updated by adding the product of a step size $\alpha$ and the gradient of the loss function with respect to the current adversarial example $x_{\text{adv}}^{(t)}$:

\begin{equation}
x_{\text{adv}}^{(t+1)} = x_{\text{adv}}^{(t)} + \alpha \cdot \nabla_x J(\theta, x_{\text{adv}}^{(t)}, y)
\end{equation}

Where:
\begin{itemize}
\item $x_{\text{adv}}^{(t)}$ is the adversarial example at iteration $t$.
\item $\alpha$ is the step size or learning rate controlling the perturbation magnitude.
\item $\nabla_x J(\theta, x_{\text{adv}}^{(t)}, y)$ is the gradient of the loss function $J$ with respect to the input $x_{\text{adv}}^{(t)}$, computed for the target model's parameters $\theta$ and the true label $y$.
\item $x_{\text{adv}}^{(t+1)}$ is the updated adversarial example at the next iteration.
\end{itemize}

\subsection{Adversarial Particle Swarm Optimization (APSO)}
\label{subsec:APSO}
Adversarial Particle Swarm Optimization (APSO) is a nature-inspired optimization algorithm used to craft adversarial examples. APSO employs swarm intelligence principles to guide a population of particles in exploring the input space and searching for adversarial examples. The objective is to find perturbations that lead to misclassification while keeping the perturbations within certain bounds to maintain perceptual similarity.

The APSO algorithm involves the following steps:
\begin{enumerate}
\item Initialization: Randomly initialize a population of particles representing potential adversarial perturbations.
\item Evaluate Fitness: Assess the fitness of each particle by measuring the loss incurred due to the perturbation.
\item Update Particle Positions: Update the positions of particles based on their previous positions, individual experience, and collective best-performing particles (global best).
\item Bound Constraints: Apply constraints to keep the perturbations within a predefined range to maintain perceptual similarity between the original and adversarial examples.
\item Termination: Repeat the update and evaluation steps for a fixed number of iterations or until a termination condition is met.
\end{enumerate}

APSO balances exploration and exploitation to efficiently search for adversarial perturbations while avoiding excessive perturbation that could lead to significant visual differences. Leveraging swarm intelligence, APSO contributes to developing powerful and robust adversarial examples, making it a valuable tool for evaluating the security of machine learning models.
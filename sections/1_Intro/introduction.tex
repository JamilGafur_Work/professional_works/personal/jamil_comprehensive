\section{Introduction}

The increased use of AI/ML across domains \cite{chahar2020systematic} has brought to light critical concerns related to Robustness \cite{goodfellow2014explaining}, Interpretability \cite{ribeiro2016should}, and Power consumption \cite{Iea},(RIP) within the ML Operation (MLOP) workflow (defined in figure \ref{fig:MLOP_current_workflow}). Despite the increasing of AI/ML in industry and academia \cite{john2021towards}, these concerns have not been addressed \cite{karamitsos2020applying}. We define robustness as the ability of a model to withstand adversarial attacks \cite{goodfellow2014explaining}, interpretability refers to the ability to explain the model's decision-making process \cite{ribeiro2016should}, and power consumption refers to the amount of energy required to train and deploy a model \cite{Iea}. 


The concerns around RIP have been highlighted by the emergence of regulations that mandates that AI/ML models be interpretable and bias-free \cite{goodman2016eu}, studies showing AI/ML models vulnerable to adversarial attacks \cite{goodfellow2014explaining}, and energy consumption of AI/ML models increasing at an alarming rate \cite{Iea,jones2018stop}.

For the concern of Interpretability, its significance can be highlighted by the emergence of regulations such as the General Data Protection Regulation (GDPR) in the European Union, which mandates that AI/ML models be interpretable and bias-free \cite{goodman2016eu}. The GDPR introduces \textit{article 22 ``Automated individual decision-making, including profiling''} which states that any automated individual (i.e., AI/ML) must not make decisions based on specific profiling and provide explanations based on its decision. Furthermore, article 22 states that they also include a section that the company must provide ``suitable measures to safeguard the data subjects' rights and freedoms and legitimate interest..''

Consequently, Kumar~et.~al. stated that ``25 out of the 28 organizations indicated that they do not have the right tools in place to secure their ML systems and are explicitly looking for guidance.'' \cite{kumar2020adversarial}. Where 22 out of the 28 were in ``security sensitive'' industries such as banking, finance, and healthcare. Furthermore, the authors stated that ``the lack of tools and techniques to secure ML systems is a significant barrier to the adoption of ML in security-sensitive domains.'' \cite{kumar2020adversarial}. While these concerns are starting to arise, companies are also focusing on the energy consumption of AI/ML models.


\begin{figure}
    \centering
    \label{fig:MLOP_current_workflow}
    \includegraphics[width=.7\textwidth]{example-image-a}
    % https://blogs.nvidia.com/blog/2020/09/03/what-is-mlops/
    \caption{Current MLOP workflow:}
\end{figure}

Jones~et.~al.~\cite{jones2018stop} has done a comprehensive study showing that by 2030 over 20\% of the electrical demand will be from AI/ML models. This coupled with the fact that neural networks are ever-growing in parameter size and complexity, the energy consumption of AI/ML models will continue to increase

In response to these pressing demands, this paper proposes a novel MLOP workflow and framework. Our workflow is designed to cyclically incorporate eXplainable AI (XAI), Adversarial Machine Learning (AML), and Energy-Efficient AI (EEAI) techniques. This can be done using the Adversarial Observation framework, which synergistically combines these techniques for ease of use and enhanced performance.


By combining these techniques into a single framework we can focus on developing algorithms that focus on the combination of problems and can be used to enhance the performance of AI/ML models in the new MLOP workflow. To our knowledge, this is the first time that these techniques have been combined into a single framework and workflow.

The significance of this research lies in its ability to provide essential tools for safeguarding AI/ML systems against adversarial attacks, establishing robust standards, and improving energy efficiency. We focus on these problems by incorporating the newly developed Adversarial Observation framework into different studies. 

The main objectives of this paper is as follows:

\begin{enumerate}
  \item Introduce eXplainable AI (XAI), Adversarial Machine Learning (AML), and Energy-Efficient AI (EEAI) techniques.
  
  \item To show a comprehensive MLOP workflow and framework that addresses the concerns of RIP.
  
  \item Discuss current projects being used to validate the effectiveness of the Adversarial Observation framework in improving the security, interpretability, and energy efficiency of machine learning models.
\end{enumerate}


The rest of this paper follows the main objectives stated above where section~\ref{sec:XAI_tech} provides an overview of XAI techniques and models that inherently incorporate XAI principles. Section~\ref{sec:AML} discusses the different AML techniques and models that can be used to enhance the security of AI/ML models. Section~\ref{sec:EE} presents the various EEAI techniques and models that can be leveraged to improve the energy efficiency of AI/ML models. Section~\ref{sec:AO_Framework} introduces the Adversarial Observation framework, which synergistically combines generative adversarial attacks and XAI techniques. Section~\ref{sec:Dev_Cycle} explores the current MLOP development cycle, identifies its limitations, and proposes an enhanced cycle that considers essential factors such as energy efficiency, interpretability, and security. Section~\ref{sec:current_work} discusses our ongoing work, with a focus on integrating the Adversarial Observation framework into the MLOP development cycle to address its limitations. Finally, Section~\ref{sec:Conclusion} concludes the paper and discusses future work.

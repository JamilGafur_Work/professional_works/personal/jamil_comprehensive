\section{Adversarial Observation Framework}
\label{sec:AO_Framework}
The Adversarial Observation framework offers a transformative solution to address RIP concerns in the current MLOP workflow. It focuses on enhancing security, interpretability, and efficiency in machine learning models, catering to the needs of various stakeholders in the domain.

The framework's key strength lies in its ability to synergistically combine generative adversarial attacks, explainable AI, and model pruning techniques. These techniques can be combined to create new algorithms that address the shortcomings of existing methods. We demonstrate the framework's versatility by showcasing three use cases that leverage the framework's modules to address various concerns in the machine learning domain.

These modules are designed to be model agnostic and can be applied to any neural network architecture. Its modular design provides a convenient interface for users to leverage the framework's capabilities. In figure \ref{fig:Usecases}, we demonstrate two use cases for the framework: (\textbf{A}) generating adversarial images to secure the network \cite{goodfellow2014explaining, xie2020adversarial}, and (\textbf{B}) generating interpretability of the model after training \cite{khalifa2020verification}.



\begin{wrapfigure}{r}{0.5\textwidth}
    \centering
    \includegraphics[width=0.45\textwidth]{./images/Adversarial_Use.png}
    \caption{Two use cases for the framework: (\textbf{A}) generating adversarial images to secure the network \cite{goodfellow2014explaining, xie2020adversarial}, and (\textbf{B}) generating interpretability of the model after training \cite{khalifa2020verification}.}
    \label{fig:Usecases}
\end{wrapfigure}


In this section, we explore three distinct use cases that showcase the framework's potential and the seamless interoperation of its modules. The first use case involves adversarial visualization, wherein deceptive examples are generated to challenge machine learning models using the APSO and activation function methods. The second use case focuses on enhancing adversarial robustness with interpretability and improving model resilience through adversarial examples with FGSM and ShAP. Lastly, the third use case revolves around feature importance, extracting crucial model features associated with specific classes using FGSM and activation mapping.

For our experiments, we utilized a Convolutional Neural Network (CNN) trained on the widely used MNIST dataset \cite{deng2012mnist}. For detailed model architecture and parameter information, please refer to the examples folder in our GitHub repository\footnote{\url{https://github.com/EpiGenomicsCode/Adversarial_Observation}}. The training process and model architecture were based on the official PyTorch MNIST documentation for Image Classification Using ConvNets\footnote{\url{https://pytorch.org/examples/}}. The model was trained for 20 epochs with a batch size of 64 and a learning rate of 0.01. The model achieved an accuracy of 99.2\% on the test set.


\subsection{Adversarial Visualization}
\label{sec:sub_Adversarial_Visualization}
\begin{figure}
    \captionsetup[subfigure]{labelformat=empty}
    \centering
    \begin{subfigure}[b]{.2\textwidth}
        \centering
        \includegraphics[width=\textwidth]{/home/jamilg/Desktop/Repos/Work/Professional_Work/Code/Adversarial_Research/example/MNIST/APSO/points/epoch_0.png}
        \caption{(\textbf{A}): initial}
        \label{fig:APSO_0}
    \end{subfigure}
    \hfill
    \begin{subfigure}[b]{.2\textwidth}
        \centering
        \includegraphics[width=\textwidth]{/home/jamilg/Desktop/Repos/Work/Professional_Work/Code/Adversarial_Research/example/MNIST/APSO/points/epoch_19.png}
        \caption{(\textbf{B}): APSO 20 epochs}
        \label{fig:APSO_20}
    \end{subfigure}
    \hfill
    \begin{subfigure}[b]{.2\textwidth}
        \centering
        \includegraphics[width=\textwidth]{/home/jamilg/Desktop/Repos/Work/Professional_Work/Code/Adversarial_Research/example/MNIST/APSO/images/epoch_0/best.png}
        \caption{(\textbf{C}): Initial Best}
        \label{fig:Best_0}
    \end{subfigure}
    \hfill
    \begin{subfigure}[b]{.2\textwidth}
        \centering
        \includegraphics[width=\textwidth]{/home/jamilg/Desktop/Repos/Work/Professional_Work/Code/Adversarial_Research/example/MNIST/APSO/images/epoch_21/best.png}
        \caption{(\textbf{D}): Best 20 epochs}
        \label{fig:Best_20}
    \end{subfigure}
    
    \caption{Visualization of the APSO after 20 epochs; (\textbf{A}) and (\textbf{B}) show the swarm initialization of particles with random noise and the swarm's convergence after 20 epochs, respectively. (\textbf{C}) and (\textbf{D}) shows the image with the highest confidence of ``3'' found by the swarm after 1 and 20 epochs, respectively.}
    \label{fig:mainfig}
\end{figure}

In this section, we demonstrate a method for generating adversarial examples using the APSO algorithm and the activation function. Specifically, we initialize the swarm with random noise and define the cost function as defined in Eq.~\ref{eq:cost}.

The cost function $\text{cost}(x)$ combines both the prediction probability and the normalized activation to determine the overall cost associated with the image. For each iteration the swarm takes, we reduce the particles to a 2-D Uniform manifold approximation (UMAP) \cite{mcinnes2018umap} space. This allows us to visualize how the particles move over time.

\begin{equation}
    \label{eq:cost}
    cost(x) = f(x)[3] * act(x)
\end{equation}

where $x$ represents the input MNIST image, $f(x)[3]$ denotes the prediction probability for the digit 3, $\text{act}(x)$ represents the activation of the input, and $\text{norm}(\cdot)$ denotes a normalization function.


The swarm iteratively searches for images with high predictive values for the ``3'' class that also have high activations for explainable visualization. We run the APSO algorithm for 20 epochs, with Fig.~\ref{fig:APSO_0} depicting the initialized swarm and Fig.~\ref{fig:APSO_20} showing the swarm after 20 epochs. For each epoch, we present the points with the highest confidence in classifying the image as ``3'', which are illustrated in figures \ref{fig:Best_0} and \ref{fig:Best_20}. 

Given the framework's broad utility, it can readily be customized to suit various purposes. Specifically, the implementation of the APSO algorithm can facilitate the generation of comparable outcomes to those reported in \cite{liu2018security, Shreeharsha2021TrainingOG}.

\subsection{Adversarial Robustness}
This section showcases the capability of our framework in enhancing the robustness of a Neural Network against adversarial noise and the updated interpretability based on ShAP values. We utilize the FGSM attack \cite{goodfellow2014explaining} to generate such noise and incorporate it into the training data as described in Mosli et al. \cite{liu2018security}. Adversarial training has proven to be an effective approach in improving a model's resilience to future attacks \cite{liu2018security,wang2021adversarial}, which involves adding an extra neuron to the output layer to classify an ``adversarial'' or ``unknown'' class. 


Our framework simplifies the generation and addition of adversarial noise to using the FGSM attack, offering an easy and efficient way to retrain the network. This technique can significantly enhance the robustness of a neural network with minimal additional effort. By incorporating adversarial noise into the training process, the model becomes better prepared to handle adversarial attacks during deployment. 

\begin{figure}
    \centering    
    \includegraphics[width=.4\textwidth]{./images/SHap.png}
    \caption{ShAP values for an adversarial secure network. The left column is the original validation image. All other columns are ShAP values for the same image with respect to different classifications (0-3)}
    \label{fig:shap}
\end{figure}

We then use the ShAP values to explain the model's new decision-making process to see if by applying this technique if the model is still able to identify the correct features. We use the same model as in the previous section, take some validation data and run it through the ShAP algorithm. This is then compared to the ShAP values of the same data after the model has been retrained with adversarial noise. The results are shown in Fig~\ref{fig:shap}.


\subsection{Feature Importance of Adversarial Images}

Our framework can also be used to generate feature importance maps for original data, and adversarial images. To do this we utilize the FGSM and the activation mapping. This approach enables us to visualize the most significant features that influence a model's decision-making process, and how adversarial noise affects what the model ``sees''. An example of this is shown in Fig~\ref{fig:fgsm}. The noise shown in Fig~\ref{fig:fgsm}~\textbf{C} is the noise multiplied by $\epsilon$ and added to the original. 


\begin{figure}
    \centering
    \includegraphics[width=0.45\textwidth]{/home/jamilg/Desktop/Repos/Work/Professional_Work/Code/Adversarial_Research/example/MNIST/attack_results/fgsm_0.001_3.png}
    \caption{Adversarial image generated using FGSM with $\epsilon = 0.001$. Left: Original image of ``3'' from the MNIST test set. Center: Adversarial image generated using FGSM. Right: Noise added to the original image to generate the adversarial image.}
    \label{fig:fgsm}
\end{figure}



The development of feature importance maps represents a significant contribution to the field of AI/ML. By enabling us to understand a model's decision-making process and how it changes under adversarial attack, we can increase accountability and build trust in machine learning systems. We show how the combination of activation mapping and the FGSM attack can be used to generate feature importance maps for adversarial images.

\begin{wrapfigure}{r}{0.5\textwidth}
    \centering
    \includegraphics[width=.45\textwidth]{/home/jamilg/Desktop/Repos/Work/Professional_Work/Code/Adversarial_Research/example/MNIST/attack_results/fgsm_0.001_3_grad.png}
    \caption{Left: The activation mapping of the original image in Fig.~\ref{fig:fgsm}. Right: The activation mapping of the Adversarial image in Fig.~\ref{fig:fgsm}}
    \label{fig:fgsm_grad}
\end{wrapfigure}
    

Our proposed end-to-end framework enables the smooth implementation of the aforementioned approach, making it both feasible and straightforward to execute. In Fig.~\ref{fig:Usecases}, we present the activation mapping of the original image alongside the adversarial image (refer to Fig.~\ref{fig:fgsm_grad}). While these images exhibit visual similarity, a careful examination reveals a subtle alteration in the center-right region of both images. This observation underscores the significant impact that minor perturbations in the overall gradients of the model can exert on its prediction outcomes. Our innovative methodology combines adversarial attacks and activation mapping, thereby providing valuable insights into the susceptibility of the model to such attacks. By analyzing the influence of adversarial noise on the decision-making process of the model, our approach enables the identification of vulnerabilities and the enhancement of the robustness of machine learning systems. The seamless integration of our proposed approach within the comprehensive end-to-end framework ensures its practicality and ease of implementation.

The utilization of feature importance maps and the identification of how gradients activate differently with malicious inputs represent a novel and effective means of enhancing the transparency and interpretability of machine learning models. Our framework simplifies the generation of these maps through activation mapping, presenting a fresh approach to comprehending the decision-making process of intricate models. Ultimately, this approach contributes to ensuring the responsible and accountable use of machine learning systems, rendering them more trustworthy and beneficial for society.